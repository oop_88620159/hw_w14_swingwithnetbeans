/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w14_swingwithnetbean;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author acer
 */
public class Computer {

    private int playerHand;
    private int hand;
    private int win;
    private int draw;
    private int lose;
    private int status;

    public Computer() {

    }

    private int choob() {

        return ThreadLocalRandom.current().nextInt(0, 3);

    }

    public int paoYingChoob(int playHand) {
        this.playerHand = playHand;
        this.hand = choob();
        if (this.playerHand == this.hand) {
            draw++;
            status = 0;
            return 0;
        }
        if (this.playerHand == 0 && this.hand == 1) {
            win++;
            status = 1;
            return 1;
        }
        if (this.playerHand == 1 && this.hand == 2) {
            win++;
            status = 1;
            return 1;
        }
        if (this.playerHand == 2 && this.hand == 0) {
            win++;
            status = 1;
            return 1;
        }
        lose++;
        status = -1;
        return -1;

    }
    public int getStatus() {
        return status;
    }

    public int getPlayerHand() {
        return playerHand;
    }

    public int getHand() {
        return hand;
    }

    public int getWin() {
        return win;
    }

    public int getDraw() {
        return draw;
    }

    public int getLose() {
        return lose;
    }
    
}

    
    
   